# Compare Azure Functions

## Projects

* CompareSpeed_TableStorage - compare speed of CRUD to TableStorage using different languages
  * Also compares writing directily to the output file vs to stdout
* HelloWorld - demonstrates a 'Hello World' for creating Azure Functions using the CLI

----------------------------------------------

## Install CLI

### Options

* Azure CLI
    * [Install page](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
    * msi includes PowerShell and cmd
    * cmd alias = `az`
* Cross-platform CLI 
    * `npm i -g azure-cli`
    * cmd alias = `azure`

### Yeoman:
```
npm i -g yo generator-azurefunctions azure-functions-cli
```

## Setup CLI

### Register Device:
1. `az login`
2. Copy code, go to (https://aka.ms/devicelogin)[https://aka.ms/devicelogin], and paste

## Setup Azure

### Azure Functions CLI

Install: `npm i -g azure-functions-core-tools`

Aliases: func, azfun, azure-functions

### Create group

Only needed for deploying

### Create user


# Create Function

## Local

### Prerequisites



### Visual Studio

#### C#
#### F#
#### Go
#### Node

### Azure CLI

Triggers are listed [here](https://github.com/Azure/azure-webjobs-sdk-templates/tree/dev/Functions.Templates/Templates).

#### C#
#### F#
#### Go

Azure CLI:
```
az functionapp create --name HelloGo --Language Batch --Template HttpTrigger
```

azure-functions-cli:
```
func function new 
```

#### Node

Notes:
    * Azure functions are hosted on as few 40 instances and as many as 300+

# Testing Azure Function (local)

```
func host start --port 8870 
```

Tip: Live updates works both for changing the live file's contents and any dependencies such as executables that the file calls. The only exception is if you are changing the live file's language. Example: changing from batch to Powershell results in needing to restart the Azure Function.

Output: the request has a requestId in the header that is a Guid. This is new folder name used in saving to the AppData folder. This folder is only created when it is written to. This is a file that is manually written to inside of the Azure Function.

## Plumbing

### Expected files
| Function Type | Trigger | Expected File Name |Query string Variables Available (from function.json) |Result file path variable (from function.json)|
| - | - | - | - | - |
|Batch|Http|run.bat|`req_query_YOURVARNAME`|%res%|
|exe|Http|run.exe|???|???|
|Go|Http|run.exe|???|???|
|PowerShell|Http|run.ps1|`$req_query_YOURVARNAME`|$res|


# Organize later 
[x] `func function new --template` uses Yeoman. Research if this is a good option.
    
    * This is from azure-functions-cli at [here](https://github.com/Azure/azure-functions-cli)
    * Yeoman is *not* a good option. The repo is down.

# TODO:

[ ] Debug Azure Function in VS Code
`func host --port 8870 --debug VSCode`
[ ] Research serverless.com
[ ] Research Azure Webjobs SDK
[ ] Manually call batch and Powershell from the command line for testing
[ ] Document list of [templates](https://github.com/Azure/azure-webjobs-sdk-templates/tree/dev/Functions.Templates/Templates) or [samples](https://github.com/Azure/azure-webjobs-sdk-script/tree/dev/sample)
[ ] Make sure VS has extension for "Azure Functions and Web Jobs Tools"