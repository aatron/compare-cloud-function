# Instructions / Fun Facts

## About

This project was created using the [Azure Function CLI](https://github.com/Azure/azure-functions-cli). It demonstrates how one host can have different languages for each function.

## How to debug

1. Host function in terminal: `func host start --port 8801 --debug vscode`
1. Attach code so breakpoints can be set: `F5`
    * ONLY the breakpoints work in JavaScript and TypeScript

## Debug Information

Debug port: `8801`
Attach port: `8800`

> They can't be on the same port #. The `func host` command can use any port other than `8800`.

## Notes

### Language Info

* CSharp
  * NuGet - Automatically pulls in NuGet packages to user folder
  * Debug: no
* FSharp
  * NuGet - Automatically pulls in NuGet packages to user folder
  * Debug: no
* JavaScript
  * Debug: yes
* TypeScript
  * Debug: yes
  * Transpile JS: `tsc -p .`

### How these language folders were generated

``` cmd
func new --language C# --template HttpTrigger --name CSharp1
func new --language F# --template HttpTrigger --name FSharp1
func new --language JavaScript --template HttpTrigger --name JavaScript3
func new --language TypeScript --template HttpTrigger --name TypeScript3
```

### Nice to haves

* VS Code Tasks
  * Azure Function Host
  * TypeScript watch to automatically transpile
* Upgrade the .NET functions to use csproj instead of project.json