# Compare Azure Functions using Table Storage

## About

Each folder is a different language, and sometimes output mechanism, that uses TableStorage for CRUD. Each function returns the same JSON so that a client can interact with each language.

## Instructions (Windows)

### Prerequisites

#### Azure CLI

Install Azure Functions CLI: `npm i -g azure-functions-core-tools`

Install the [Azure Storage Emulator](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-emulator)

#### Go

Install the Azure SDK for Go:

``` cmd
go get -u github.com/Azure/azure-sdk-for-go
go get -u github.com/Azure/go-autorest/autorest
go get -u github.com/Azure/go-autorest/autorest/azure
go get -u github.com/Azure/go-autorest/autorest/date
go get -u github.com/Azure/go-autorest/autorest/to
go get -u github.com/satori/uuid
```

[Azure SDK for Go repository](https://github.com/Azure/azure-sdk-for-go) and [documentation](https://godoc.org/github.com/Azure/azure-sdk-for-go)

#### JavaScript

Install npm

#### TypeScript

Install TypeScript: `npm i -g TypeScript`

### Run (local development)

> Fun fact: For local development, there can be live changes both to the Powershell script and the Go .exe output without restarting the Azure Function.

Have your app call the Azure Function or use an app like [Postman](https://www.getpostman.com/) or [Fiddler](http://www.telerik.com/fiddler/) to send messages (HTTP Triggers) to the Azure Function.

> Using VS Code, the breakpoints only work in JavaScript and TypeScript.

#### VS Code Task

Run task: `host all`

> ALL of the Azure Functions will be run from this command.

#### Terminal

`func host start --port 8871`

#### Test URL's

* Go - write to the output file directly: GET - `http://localhost:8871/api/go_file_powershell?name=spongebob4`
* Go - write out to stdout and have Powershell write to the output file: POST - `http://localhost:8871/api/go_stdout_powershell`
  * Body:
  ``` json
  {
      "name": "Spongebob"
  }
  ```
* 

### Build

All of the build commands are for each individual language. However, when the code runs, all functions are available.

You can build in the Terminal or VS Code.

#### VS Code Tasks

* `Build - Go - Direct to File`

#### Terminal

``` cmd
cd .\go_file_powershell\GoMain
go build -o ../HelloGo/runGo.exe
```

## Cross-Platform

Cross platform tooling not supported for local Azure Functions, [yet](https://github.com/Azure/azure-webjobs-sdk-script/wiki/Azure-Functions-runtime-2.0-known-issues).

Research:

* Azure Functions on Linux: https://blogs.msdn.microsoft.com/appserviceteam/2017/09/25/develop-azure-functions-on-any-platform/
