# Instructions 

## Windows

### Prerequisites

Install Azure Functions CLI: `npm i -g azure-functions-core-tools`

Install the [Azure Storage Emulator](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-emulator)

Install the Azure SDK for Go:

``` cmd
go get -u github.com/Azure/azure-sdk-for-go
go get -u github.com/Azure/go-autorest/autorest
go get -u github.com/Azure/go-autorest/autorest/azure
go get -u github.com/Azure/go-autorest/autorest/date
go get -u github.com/Azure/go-autorest/autorest/to
go get -u github.com/satori/uuid
```

[Azure SDK for Go repository](https://github.com/Azure/azure-sdk-for-go) and [documentation](https://godoc.org/github.com/Azure/azure-sdk-for-go)

### Build

You can build in the Terminal or VS Code.

#### Terminal

##### Build Go code

```
cd Code
go build -o ../HelloGo/runGo.exe
```

#### VS Code

Steps:

* Open folder `go_powershell` in VS Code
* TODO: Create VS Debug file with Go output path

### Run

> Fun fact: There can be live changes both to the Powershell script and the Go .exe output.

Have your app call the Azure Function or use an app like [Postman](https://www.getpostman.com/) or [Fiddler](http://www.telerik.com/fiddler/) to send messages to the Azure Function.

#### Host Azure Function locally

`func host start --port 8870`

### Debug

#### PowerShell

TODO: Set breakpoint

#### Go

TODO: Remote debug?

## Deploy

### Prerequisites

Need to create the following:

* Resource Group
* App Service Plan
* Storage Account

> Tip: Create these in Azure ahead of time. Visual Studio will offer to create these for you but I don't recommend it. There is currently a bug (feature?) that you can't choose a Consumption App Service Plan.

## Cross-Platform

Cross platform tooling not supported for local Azure Functions, [yet](https://github.com/Azure/azure-webjobs-sdk-script/wiki/Azure-Functions-runtime-2.0-known-issues).

Research:

* Azure Functions on Linux: https://blogs.msdn.microsoft.com/appserviceteam/2017/09/25/develop-azure-functions-on-any-platform/
* 