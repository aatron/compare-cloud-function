package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"time"

	azureLib "gitlab.com/aatron/compare-cloud-function/go_stdout_powershell/GoAzureLib"
)

func main() {
	startInit := time.Now().Format("2006-01-02 15:04:05.9999999999")

	// Get args passed into the exe
	reqBodyPtr := flag.String("req", "default request", "path to the file used for input")
	azName := flag.String("azName", "", "Name for the Azure account")
	azKey := flag.String("azKey", "", "Key for the Azure account")

	flag.Parse()

	queryResults := azureLib.QueryPerson(*azName, *azKey, *reqBodyPtr)
	queryResults.StartInit = startInit

	queryResultsBytes, err := json.Marshal(queryResults)
	if err != nil {
		panic(err)
	}

	// jsonString := bytes.IndexByte(queryResultsBytes, 0)
	jsonString := string(queryResultsBytes[:])

	// Write to stdout
	fmt.Print(jsonString)
}
