############# Azure Function Variables ##############
# $req = File path to the request text file
# $res = File path to the result text file
#####################################################

############# Azure Environment Variables ##############
# ${env:AzName} = Azure Table Storage connection name
# ${env:AzKey} = Azure Table Storage connection key
########################################################

$startScript = Get-Date
$dateFormat = "yyyy-MM-dd hh:mm:ss.fffffff"

$requestBody = Get-Content $req -Raw | ConvertFrom-Json

# Sample of getting a value from the request body
#$name = $requestBody.name

# Sample of getting a value from a query string. Each querystring parameter is its own variable
# if ($req_query_name2) 
# {
#     $name = $req_query_name2 
# }

$afterParse = Get-Date

# Call exe (Go is responsible for outputting the response)
if (${env:HOME}) {
    $exePath = "${env:HOME}\site\wwwroot\Go_Stdout_Powershell\GoMain\mainGo.exe"
    $exeOutput = & $exePath -res $res -req $requestBody -azName "${env:AzName}" -azKey "${env:AzKey}"
}
else {
    # For local development. Uses the Table Storage emulator.
    $exePath = Join-Path $pwd.Path "Go_Stdout_Powershell\GoMain\mainGo.exe"
    $exeOutput = & $exePath -res $res -req $requestBody
}

$afterExe = Get-Date

# Parse JSON
$allJson = @{
    Name = "Powershell wrapper around Go"
    StartScript = $startScript.ToString($dateFormat)
    AfterArgParse = $afterParse.ToString($dateFormat)
    AfterExe = $afterExe.ToString($dateFormat)
    TotalDuration = (New-TimeSpan -Start $startScript -End $afterExe).Milliseconds
    Events = ($exeOutput | ConvertFrom-Json)
}

# Output json to response
Out-File -Encoding Ascii -FilePath $res -inputObject ([string]($allJson | ConvertTo-Json))