package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	azureLib "gitlab.com/aatron/compare-cloud-function/go_file_powershell/GoAzureLib"
)

// TODO: Lots of the error messages won't show up because they route to stdout instead of writing to the file directly
func main() {
	startTime := time.Now()
	startInit := time.Now().Format("2006-01-02 15:04:05.9999999999")

	// Get args passed into the exe
	resPtr := flag.String("res", "default result", "path to the file for writing output to")
	reqBodyPtr := flag.String("req", "default request", "path to the file used for input")
	azName := flag.String("azName", "", "Name for the Azure account")
	azKey := flag.String("azKey", "", "Key for the Azure account")

	flag.Parse()

	queryResults := azureLib.QueryPerson(*azName, *azKey, *reqBodyPtr)
	queryResults.StartInit = startInit

	current := time.Now()
	duration := current.Sub(startTime)
	queryResults.TotalDuration = float64(duration) / float64(time.Millisecond)

	queryResultsBytes, err := json.Marshal(queryResults)
	if err != nil {
		panic(err)
	}

	// Write to output file
	ioutil.WriteFile(*resPtr, queryResultsBytes, 0644)
}

func writeOutput(message string) {
	fmt.Println(message)
}

func writeOutputFileDirectly(file *os.File, message string) {
	if _, err := file.WriteString(message + "\n"); err != nil {
		panic(err)
	}
}
