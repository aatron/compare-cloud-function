# Azure Function Sample (Go)

## About

Go, called by Powershell, explicitly writes to the output file.

## Instructions (Windows)

See instructions in the parent's [README.md](../README.md).

### Prerequisites

Install the Azure SDK for Go:

``` cmd
go get -u github.com/Azure/azure-sdk-for-go
go get -u github.com/Azure/go-autorest/autorest
go get -u github.com/Azure/go-autorest/autorest/azure
go get -u github.com/Azure/go-autorest/autorest/date
go get -u github.com/Azure/go-autorest/autorest/to
go get -u github.com/satori/uuid
```

[Azure SDK for Go repository](https://github.com/Azure/azure-sdk-for-go) and [documentation](https://godoc.org/github.com/Azure/azure-sdk-for-go)

### Build

You can build in the Terminal or VS Code.

#### Terminal

##### Build Go code

``` cmd
cd Code
go build -o ../HelloGo/runGo.exe
```

#### VS Code

Run task: `Build - Go - Direct to File`