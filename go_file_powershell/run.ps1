############# Azure Function Variables ##############
# $req = File path to the request text file
# $res = File path to the result text file
#####################################################

############# Azure Environment Variables ##############
# ${env:AzName} = Azure Table Storage connection name
# ${env:AzKey} = Azure Table Storage connection key
########################################################

# POST method: $req
$requestBody = Get-Content $req -Raw | ConvertFrom-Json
$name = $requestBody.name

# GET method: each querystring parameter is its own variable
if ($req_query_name2) 
{
    $name = $req_query_name2 
}

# Call exe (Go is responsible for outputting the response)
if (${env:HOME}) {
    $exePath = "${env:HOME}\site\wwwroot\go_file_powershell\GoMain\mainGo.exe"
    & $exePath -res $res -req $requestBody -azName "${env:AzName}" -azKey "${env:AzKey}"
}
else {
    # For local development. Uses the Table Storage emulator.
    $exePath = Join-Path $pwd.Path "go_file_powershell\GoMain\mainGo.exe"
    & $exePath -res $res -req $requestBody
}