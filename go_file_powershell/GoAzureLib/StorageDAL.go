package storagedal

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/Azure/azure-sdk-for-go/storage"
)

var (
	accountName      string
	accountKey       string
	storageClient    storage.TableServiceClient
	resPtr           *string
	reqBodyPtr       *string
	emulator         *bool
	events           Events
	formatDateString = "2006-01-02 15:04:05.9999999999"
)

// TODO: Convert to slice for each event
type Events struct {
	StartInit     string // `json:"startInit"`
	BeforeClient  string
	AfterClient   string
	BeforeQuery   string
	AfterQuery    string
	BeforeParse   string
	AfterParse    string
	Result        string
	TotalDuration float64
}

type Person struct {
	FirstName  string
	MiddleName string
	LastName   string
}

// TODO: Create common library to be shared between all of the Go projects
func QueryPerson(azName string, azKey string, reqBody string) Events {
	accountName = azName
	accountKey = azKey

	if accountName == "" || accountKey == "" {
		accountName = storage.StorageEmulatorAccountName
		accountKey = storage.StorageEmulatorAccountKey
	}

	// Instantiate TableServiceClient
	events.BeforeClient = time.Now().Format(formatDateString)
	storageClient = getTableClient(accountName, accountKey)
	events.AfterClient = time.Now().Format(formatDateString)

	// Get the table reference
	personTable := storageClient.GetTableReference("Person")

	options := storage.QueryOptions{
		Top: 1,
	}

	events.BeforeQuery = time.Now().Format(formatDateString)

	// Query the Azure Storage table
	p, perr := personTable.QueryEntities(30, storage.NoMetadata, &options)
	if perr != nil {
		panic(perr)
	}

	events.AfterQuery = time.Now().Format(formatDateString)

	events.BeforeParse = time.Now().Format(formatDateString)
	person := p.Entities[0]

	// Parse body example:
	thePerson := Person{
		FirstName:  person.RowKey,
		MiddleName: person.Properties["MiddleName"].(string),
		LastName:   person.PartitionKey}

	events.AfterParse = time.Now().Format(formatDateString)

	// Convert results to json
	byteEvents, err := json.Marshal(thePerson)
	if err != nil {
		panic(err)
	}

	events.Result = string(byteEvents)

	return events
}

func getTableClient(accountName string, accountKey string) storage.TableServiceClient {
	client, err := storage.NewBasicClient(accountName, accountKey)
	onErrorFail(err, "Create client failed")

	return client.GetTableService()
}

// onErrorFail prints a failure message and exits the program if err is not nil.
func onErrorFail(err error, message string) {
	if err != nil {
		fmt.Printf("%s: %s\n", message, err)
		os.Exit(1)
	}
}
