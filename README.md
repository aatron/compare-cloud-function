# Compare Azure Cloud Functions

![Banner](/assets/banner.jpg)

## Summary

Azure Functions can run more languages than anyone else. Tied with Google for the cheapest option. Tooling only works on Windows and Linux tooling is in development.

### Comparison Table

|Language|Debug?|Databases|Status|Notes|
|-|-|-|-|-|-|-|-|-|-|-|-|-|
|C#|Yes|<ul><li>Table Storage</li><ul><li>**Total**: 50ms</li></ul>|Supported|Yep...they like C#|
|F#|??|TODO|Supported|MS + F# = BFF#|
|CSX|??|TODO|Supported|TODO|
|.NET Library|??|TODO|Supported|TODO|
|Go (Batch)|??|TODO|Experimental|TODO|
|Go (Bash)|??|TODO|Experimental|TODO|
|Go (Powershell)|No|<ul><li>Table Storage</li><ul><li>Init client: 600ms</li><li>Query: 250ms</li><li>Http & call exe: 650ms</li><li>**Total**: 1500ms</li></ul>|Experimental|Slow.|
|Node|??|TODO|Experimental|TODO|
|Python|??|TODO|Experimental|TODO|

## Commands

`"C:\Program Files (x86)\Microsoft SDKs\Azure\Storage Emulator\AzureStorageEmulator.exe" start`

## Test local

Post
{"name": "Spongebob Squarepants"}

* Tooling
  * TODO: Create functions
  * TODO: Debug functions

## Tooling

### CLI

#### Azure Function CLI

Install CLI: `npm i -g azure-functions-core-tools`

##### Create Function

Commands used to create the functions:

``` cmd
func new --language C# --name CSharp1
func new --language F# --name FSharp1
func new --language Powershell --name go_file_powershell
func new --language Powershell --name go_stdout_powershell
func new --language JavaScript --name JavaScript3
func new --language TypeScript --name TypeScript3
```

#### Deploy

To deploy using a local git repository:

* Select the External Git option in the Azure Portal
  * Copy the git path
* In Git, locally create a remote called 'azure' with the path that you copied.
* To deploy: `git push azure master`

> Important: this push will deploy to your Azure git repository, NOT your code repository. You also need to call `git push` to persist your changes.

More details in the 'Lessons Learned' section.

#### Table Storage Emulator

The [Table Storage Emulator](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-emulator) is a wrapper around SQL Server and [here](https://docs.microsoft.com/en-us/azure/storage/common/storage-use-emulator) are the install instructions.

To run: run `Azure Storage Emulator` from the start menu.

> Note: I tried to set-up a VS Code Task for this but couldn't get it to work.

### VS Code

#### Build

* `Ctrl + Shift + B` - Choose Task (project) to build.
* Run Task - Choose Task (project) to build.

#### Run

Run Task `host_function`

Each folder has an Azure Function in a different language. This command will start all of them at the same time.

#### Test

``` cmd
TODO
```

### Visual Studio

To create a new C# Azure Function: Create new Azure Function project and then select the trigger.

> Tip: When deploying from Visual Studio, have all dependencies such as Resource Group already created. I ran into bugs when I tried to have Visual Studio create these items for me.

> Fun fact: Visual Studio Community supports Azure Functions.

## Lessons Learned

* Deployment
  * Git Deployment
    * Can deploy with automatic GitHub hook or push using local git repository to the Azure Function Git remote destination.
      * Since I use GitLab, I utilize the local git repository option.
    * Dependencies auto downloaded
      * npm and NuGet dependencies are downloaded automatically
    * Folder structure matters
      * Visual Studio projects reign supreme
        * A VS project with Azure Function(s) that is  a sibling of other projects that are "folder  based" will result in the other projects no  longer being available.
        * Even if I put the VS project in a subfolder, it still hides the others.
* Powershell - preliminary results suggest that Powershell is a slower option than others.
   * It was significantly slower doing the same operation in Powershell + Go + TableStorage vs C# + TableStorage.
   * It seems that PowerShell itself is slower and calling an exe might add unexpected additional time.

## Attribution

* [SoftSource Consulting](http://www.sftsrc.com/) - Many thanks for contributing to this project
* [NPYL Digital Collections](https://digitalcollections.nypl.org/) - Thanks for providing the banner